package org.gabmus.universityexercise.server;

import static org.gabmus.universityexercise.StudentProtoOuterClass.*;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class University {

    public static void main(String[] args) throws Exception {
        ServerSocket serverSocket = new ServerSocket(1337);
        List<StudentProto> studentsList = new ArrayList<>();
        while (true) {
            Socket receiveSocket = serverSocket.accept();
            StudentProto student = StudentProto.parseFrom(receiveSocket.getInputStream());
            System.out.println(student);
            studentsList.add(student);
        }
    }
}
