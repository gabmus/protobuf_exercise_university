package org.gabmus.universityexercise.client;

import static org.gabmus.universityexercise.StudentProtoOuterClass.*;

import java.net.Socket;

public class Student {

    public static void main(String[] args) throws Exception {
        Socket socket = new Socket("localhost", 1337);

        StudentProto student = StudentProto.newBuilder()
                .setFirstname("Foo")
                .setLastname("Bar")
                .setBirthyear(1990)
                .setResidence(
                        StudentProto.Residence.newBuilder()
                                .setCity("somewhere")
                                .setAddress("something 123").build()
                )
                .addExams(
                        StudentProto.Exam.newBuilder()
                                .setSubjectName("SDP")
                                .setGrade(22)
                                .setVerbalizationDay(10)
                                .setVerbalizationMonth(9)
                                .setVerbalizationYear(2020)
                                .build()
                )
                .build();

        student.writeTo(socket.getOutputStream());
        socket.close();
    }
}
